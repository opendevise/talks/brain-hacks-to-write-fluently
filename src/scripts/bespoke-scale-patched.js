module.exports = function(options) {
  return function(deck) {
    var parent = deck.parent,
      firstSlide = deck.slides[0],
      slideHeight = firstSlide.offsetHeight,
      slideWidth = firstSlide.offsetWidth,

      wrap = function(element) {
        var wrapper = document.createElement('div');
        wrapper.className = 'bespoke-scale-parent';
        element.parentNode.insertBefore(wrapper, element);
        wrapper.appendChild(element);
        return wrapper;
      },

      elements = deck.slides.map(wrap),

      scale = function(ratio, element) {
        element.style.transform = 'scale(' + ratio + ')';
      },

      scaleAll = function() {
        var fullScreenAdjustment = ('MozAppearance' in parent.style) && window.fullScreen ? 1 : 0;
        var xScale = parent.offsetWidth / slideWidth,
          yScale = (parent.offsetHeight + fullScreenAdjustment) / slideHeight;

        elements.forEach(scale.bind(null, Math.min(xScale, yScale)));
      };

    window.addEventListener('resize', scaleAll);
    scaleAll();
  };
};
