'use strict'

const pkg = require('./package.json')
const autoprefixer = require('gulp-autoprefixer')
const browserify = require('browserify')
const buffer = require('vinyl-buffer')
const connect = require('gulp-connect')
const csso = require('gulp-csso')
const del = require('del')
const exec = require('gulp-exec')
const git = require('git-rev-sync')
const gulp = require('gulp')
const gutil = require('gulp-util')
const path = require('path')
const plumber = require('gulp-plumber')
const rename = require('gulp-rename')
const source = require('vinyl-source-stream')
const stylus = require('gulp-stylus')
const through = require('through')
const uglify = require('gulp-uglify-es').default
const isDist = process.argv.indexOf('serve') === -1

// browserifyPlumber fills the role of plumber() when working with browserify
const browserifyPlumber = function (e) {
  if (isDist) throw e
  gutil.log(e.stack)
  this.emit('end')
}

gulp.task('js', ['clean:js'], () =>
  // see https://wehavefaces.net/gulp-browserify-the-gulp-y-way-bb359b3f9623
  browserify('src/scripts/main.js').bundle()
    .on('error', browserifyPlumber)
    .pipe(source('src/scripts/main.js'))
    .pipe(buffer())
    .pipe(isDist ? uglify() : through())
    .pipe(rename('build.js'))
    .pipe(gulp.dest('public/build'))
    .pipe(connect.reload()))

gulp.task('html', ['clean:html'], () =>
  gulp.src('src/index.adoc')
    .pipe(isDist ? through() : plumber())
    .pipe(exec('bundle exec asciidoctor-bespoke -o - src/index.adoc', { pipeStdout: true }))
    .pipe(exec.reporter({ stdout: false }))
    .pipe(rename('index.html'))
    .pipe(gulp.dest('public'))
    .pipe(connect.reload()))

gulp.task('css', ['clean:css'], () =>
  gulp.src('src/styles/main.styl')
    .pipe(isDist ? through() : plumber())
    .pipe(stylus({ 'include css': true, paths: ['./node_modules'] }))
    .pipe(autoprefixer({ browsers: ['last 2 versions'], cascade: false }))
    .pipe(isDist ? csso() : through())
    .pipe(rename('build.css'))
    .pipe(gulp.dest('public/build'))
    .pipe(connect.reload()))

gulp.task('images', ['clean:images'], () =>
  gulp.src('src/images/**/*')
    .pipe(gulp.dest('public/images'))
    .pipe(connect.reload()))

gulp.task('fonts', ['clean:fonts'], () =>
  gulp.src('src/fonts/*')
    .pipe(gulp.dest('public/fonts'))
    .pipe(connect.reload()))

gulp.task('clean', () => del.sync('public'))

gulp.task('clean:html', () => del('public/index.html'))

gulp.task('clean:js', () => del('public/build/build.js'))

gulp.task('clean:css', () => del('public/build/build.css'))

gulp.task('clean:images', () => del('public/images'))

gulp.task('clean:fonts', () => del('public/fonts'))

gulp.task('connect', ['build'], () =>
  connect.server({ root: 'public', port: process.env.PORT || 9000, livereload: false }))

gulp.task('watch', () => {
  gulp.watch('src/**/*.adoc', ['html'])
  gulp.watch('src/scripts/**/*.js', ['js'])
  gulp.watch('src/styles/**/*.styl', ['css'])
  gulp.watch('src/images/**/*', ['images'])
  gulp.watch('src/fonts/*', ['fonts'])
})

gulp.task('build', ['js', 'html', 'css', 'images', 'fonts'])

gulp.task('serve', ['connect', 'watch'])

gulp.task('default', ['build'])
